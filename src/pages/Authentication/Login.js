import PropTypes from 'prop-types'
import MetaTags from 'react-meta-tags';
import React from "react"

import { Row, Col, CardBody, Card, Alert, Container } from "reactstrap"

// Redux
import { connect } from "react-redux"
import { withRouter, Link } from "react-router-dom"

// availity-reactstrap-validation
import { AvForm, AvField } from "availity-reactstrap-validation"

// actions
import { loginUser, apiError } from "../../store/actions"

// import images
import logoLightPng from "../../assets/images/logo-light.png"
import logoDark from "../../assets/images/logo-dark.png"
import {post} from "../../helpers/api_helper";
import * as url from "../../helpers/url_helper";
import {userAuth} from "../../helpers/backend_helper";
// import {LinearProgress} from "@mui/material";

const Login = props => {
  // handleValidSubmit
  const [messagePassword, setMessagePassword] = React.useState('');
  const [messageUsername, setMessageUsername] = React.useState('');
  const [loading, setLoading] = React.useState(false);

  const handleValidSubmit = async (event, values) => {
    setLoading(true)
    const settings = {
      method: 'POST',
      headers: JSON.parse(process.env.REACT_APP_AUTH_HEADER),
      body: JSON.stringify(values)
    };
    const request = await fetch(`${process.env.REACT_APP_TG_BE_API}/user.auth`, settings);
    const response = await request.json();

    console.log(response);

    if(response.code=='201') {
      localStorage.setItem("authUser", JSON.stringify(response));
      props.history.push("/dashboard");
      setLoading(false)
    } else if(response.code=='500') {
      setLoading(false)
      return setMessagePassword('Wrong password');
    } else if(response.code=='404') {
      setLoading(false)
      return setMessageUsername('Username not found');
    }

    setMessagePassword('');
    setMessageUsername('');
    //props.loginUser(values, props.history)
  }

  return (
    <React.Fragment>
      <MetaTags>
        <title>Login | Attendance</title>
      </MetaTags>
      <div className="account-pages my-5 pt-sm-5">
        <Container>
        {/*{loading &&*/}
        {/*    <div style={{padding: "0px 335px"}}>*/}
        {/*      <LinearProgress color={"secondary"} />*/}
        {/*    </div>*/}
        {/*}*/}
          <Row className="justify-content-center">
            <Col md={8} lg={6} xl={5}>
              <Card className="overflow-hidden">
                <CardBody className="pt-0">
                  <h3 className="text-center mt-5 mb-4">
                    <Link to="/" className="d-block auth-logo">
                      <img src={logoDark} alt="" height="30" className="auth-logo-dark" />
                      <img src={logoLightPng} alt="" height="30" className="auth-logo-light" />
                    </Link>
                  </h3>
                  <div className="p-3">
                    <h4 className="text-muted font-size-18 mb-1 text-center">Welcome Back !</h4>
                    <p className="text-muted text-center">Sign in to continue to Attendance UI.</p>
                    <AvForm
                      className="form-horizontal mt-4"
                      onValidSubmit={(e, v) => {
                        handleValidSubmit(e, v)
                      }}
                    >
                      {props.error && typeof props.error === "string" ? (
                        <Alert color="danger">{props.error}</Alert>
                      ) : null}

                      <div className="mb-3">
                        <AvField
                          name="username"
                          label="Username"
                          value=""
                          className="form-control"
                          placeholder="Enter username"
                          type="text"
                          required
                        />
                      </div>
                      {messageUsername && <div style={{color:'red'}}>{messageUsername}</div>}

                      <div className="mb-3">
                        <AvField
                          name="password"
                          label="Password"
                          value=""
                          type="password"
                          required
                          placeholder="Enter Password"
                        />
                      </div>
                      {messagePassword && <div style={{color:'red'}}>{messagePassword}</div>}

                      <div className="mb-3 row mt-4">
                        <div className="col-6">
                          <div className="form-check">
                            <input
                              type="checkbox"
                              className="form-check-input"
                              id="customControlInline"
                            />
                            <label
                              className="form-check-label"
                              htmlFor="customControlInline"
                            >
                              Remember me
                            </label>
                          </div>
                        </div>
                        <div className="col-6 text-end">
                          <button className="btn btn-primary w-md waves-effect waves-light" type="submit">Log In</button>
                        </div>
                      </div>
                      <div className="form-group mb-0 row">
                        <div className="col-12 mt-4">
                          <Link to="/forgot-password" className="text-muted"><i className="mdi mdi-lock"></i> Forgot your password?</Link>
                        </div>
                      </div>

                    </AvForm>
                  </div>
                </CardBody>
              </Card>
              <div className="mt-5 text-center">
                <p>
                  Don&#39;t have an account ?{" "}
                  <Link
                    to="register"
                    className="text-primary"
                  >
                    {" "}
                    Signup now{" "}
                  </Link>{" "}
                </p>
                <p>
                  © {new Date().getFullYear()} Attendance UI
                  <span className="d-none d-sm-inline-block"> - Crafted with <i className="mdi mdi-heart text-danger"></i> by Themesbrand.</span>
                </p>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </React.Fragment>
  )
}

const mapStateToProps = state => {
  const { error } = state.Login
  return { error }
}

export default withRouter(
  connect(mapStateToProps, { loginUser, apiError })(Login)
)

Login.propTypes = {
  error: PropTypes.any,
  history: PropTypes.object,
  loginUser: PropTypes.func,
}