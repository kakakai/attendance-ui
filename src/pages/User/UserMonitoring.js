import React,{useEffect} from "react"
import MetaTags from 'react-meta-tags';
import { MDBDataTable } from "mdbreact"
import { Row, Col, Card, CardBody, CardTitle } from "reactstrap"

import { connect } from "react-redux";

//Import Action to copy breadcrumb items from local state to redux state
import { setBreadcrumbItems } from "../../store/actions";

import "../Tables/datatables.scss"

const defaultData = {
    columns: [
        {
            label: "ID",
            field: "id",
            sort: "asc",
            width: 150,
        },
        {
            label: "Name",
            field: "name",
            sort: "asc",
            width: 270,
        },
        {
            label: "Email",
            field: "email",
            sort: "asc",
            width: 200,
        },
        {
            label: "Type",
            field: "type",
            sort: "asc",
            width: 200,
        },
        {
            label: "Status",
            field: "status",
            sort: "asc",
            width: 100,
        },
    ],
    rows: []
}

const UserMonitoring = (props) => {
    const breadcrumbItems = [
        { title: "Attendance", link: "#" },
        { title: "Tables", link: "#" },
        { title: "User", link: "#" },
    ]

    const [data, setData] = React.useState({...defaultData})
    const [listUser, setListUser] = React.useState([])
    const [limit, setLimit] = React.useState(10)

    useEffect(() => {
        props.setBreadcrumbItems('User Monitoring', breadcrumbItems)
        getData()
    }, [])

    const getData = async () => {
        const settings = {
            method: 'POST',
            headers: JSON.parse(process.env.REACT_APP_AUTH_HEADER),
            body: JSON.stringify({
                "limit": limit,
                "offset": 0,
                "params": {"userStatus" : "ACTIVE"}
            })
        };
        let rows = []
        const request = await fetch(`${process.env.REACT_APP_ATD_BE_API}/get.user.by.status`, settings)
        const response = await request.json();
        if (response && response.status === "S" && response?.data) {
            let datas = response?.data?.datas
            datas && datas.map(item => {
                const {userID, userName, email, userType, userStatus} = item
                rows.push({
                    id: userID,
                    name: userName,
                    email: email,
                    type: userType,
                    status: userStatus
                })
            })
            setData({...data, rows})
            setListUser(datas)
        }
        console.log("get user by status ===>", response)
    }

    return (
        <React.Fragment>

            <MetaTags>
                <title>User Monitoring | Attendance UI</title>
            </MetaTags>

            <Row>
                <Col className="col-12">
                    <Card>
                        <CardBody>
                            <CardTitle className="h4">User Monitoring </CardTitle>
                            <p className="card-title-desc">
                                mdbreact DataTables has most features enabled by default, so
                                all you need to do to use it with your own tables is to call
                                the construction function:{" "}
                                <code>&lt;MDBDataTable /&gt;</code>.
                            </p>

                            <MDBDataTable responsive bordered data={data} entriesOptions={[5, 10, 25, 50, 100]} entries={5}/>
                        </CardBody>
                    </Card>
                </Col>
            </Row>

        </React.Fragment>
    )
}

export default connect(null, { setBreadcrumbItems })(UserMonitoring);