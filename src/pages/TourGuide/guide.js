import React,{useEffect, useState} from "react"
import MetaTags from 'react-meta-tags';
import { MDBDataTable } from "mdbreact"
import { Row, Col, Card, CardBody, CardTitle, Modal, Button } from "reactstrap"

import { connect } from "react-redux";

//Import Action to copy breadcrumb items from local state to redux state
import { setBreadcrumbItems } from "../../store/actions";

import "./datatables.scss"
import SweetAlert from "react-bootstrap-sweetalert";



const DatatableTables = (props) => {
  
  const [modal_standard, setmodal_standard] = useState(false);
  const [modal_standard_update, setmodal_standard_update] = useState(false);
  const [guides, setGuides] = useState([]);
  const [guide, setGuide] = useState([]);
  const [usersForRender, setUsersForRender] = useState([]);
  const [confirm_alert, setconfirm_alert] = useState(false);
  const [custom_div1, setcustom_div1] = useState(false);
  const [custom_div2, setcustom_div2] = useState(false);
  const [custom_div3, setcustom_div3] = useState(false);

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [ktp, setKtp] = useState("");
  const [mobileNumber, setMobileNumber] = useState("");
  const [message, setMessage] = useState("");
  const [currentPage, setCurrentPage] = useState(0);
  const [pages, setPages] =useState([]);

  let handleSubmit = async (e) => {
    try {
      let res = await fetch("https://tour-guide-masterdata.herokuapp.com/post.guide", {
        method: "POST",
        headers: {
          'Cache-Control': 'no-cache',
          'Accept': 'application/json;charset=UTF-8',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.5BG9SEVOGo_xRhtT8IkyoSy60kPg8HM9Vpvb0TdNew4'
        },
        body: JSON.stringify({
          guideID: "GUIDE-" + Date.now(),
          guideName: name,
          guideEmail: email,
          guidePhoneNumber: mobileNumber,
          guideKTPNumber: ktp,
          guideAVGRating: 0.0,
          guidePhotoURL: "",
          guideDateOfBirth: null,
          guideJoinDate: null,
          guideLanguage: [],
          isBooked: false,
          guideGender: null,
          guideStatus: "ACTIVE",
          guideCreational: {
            createdBy: "ADMIN",
            createdDate: null,
            updatedBy: "ADMIN",
            updatedDate: null,
          },
        }),
      });
      let resJson = await res.json();
      if (res.status === 200) {
        setName("");
        setEmail("");
        setKtp("");
        setMobileNumber("");
        setMessage("Guide created successfully");
        setcustom_div1(true);
        tog_standard();

        let resGet = await fetch("https://tour-guide-masterdata.herokuapp.com/get.all.guide", {
          method: 'POST',
          headers: {
            'Cache-Control': 'no-cache',
            'Accept': 'application/json;charset=UTF-8',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.5BG9SEVOGo_xRhtT8IkyoSy60kPg8HM9Vpvb0TdNew4'
          },
          body: JSON.stringify({ limit: 10, offset: 0, params: {param:'', status:'ACTIVE'} })
        })
          .then((res) => res.json())
          .then((res) => {
            setGuides(res);
          });
      } else {
        setMessage("Some error occured");
      }
    } catch (err) {
      console.log(err);
    }
  };

  let handleUpdate = async (e) => {
    try {
      let res = await fetch("https://tour-guide-masterdata.herokuapp.com/update.guide", {
        method: "PUT",
        headers: {
          'Cache-Control': 'no-cache',
          'Accept': 'application/json;charset=UTF-8',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.5BG9SEVOGo_xRhtT8IkyoSy60kPg8HM9Vpvb0TdNew4'
        },
        body: JSON.stringify({
          guideID: guide,
          guideName: name,
          guideEmail: email,
          guidePhoneNumber: mobileNumber,
          guideKTPNumber: ktp,
          guideAVGRating: 0.0,
          guidePhotoURL: "",
          guideDateOfBirth: null,
          guideJoinDate: null,
          guideLanguage: [],
          isBooked: false,
          guideGender: null,
          guideStatus: "ACTIVE",
          guideCreational: {
            createdBy: "ADMIN",
            createdDate: null,
            updatedBy: "ADMIN",
            updatedDate: null,
          },
        }),
      });
      let resJson = await res.json();
      if (res.status === 200) {
        setGuide("");
        setName("");
        setEmail("");
        setKtp("");
        setMobileNumber("");
        setMessage("Guide updated successfully");
        setcustom_div3(true);
        tog_standard_update();

        let resGet = await fetch("https://tour-guide-masterdata.herokuapp.com/get.all.guide", {
          method: 'POST',
          headers: {
            'Cache-Control': 'no-cache',
            'Accept': 'application/json;charset=UTF-8',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.5BG9SEVOGo_xRhtT8IkyoSy60kPg8HM9Vpvb0TdNew4'
          },
          body: JSON.stringify({ limit: 10, offset: 0, params: {param:'',status:'ACTIVE'} })
        })
          .then((res) => res.json())
          .then((res) => {
            setGuides(res);
          });
      } else {
        setMessage("Some error occured");
      }
    } catch (err) {
      console.log(err);
    }
  };

  function tog_standard() {
    setmodal_standard(!modal_standard)
    removeBodyCss()
  }

  function tog_standard_update(id) {
    if(modal_standard_update == false) {
      getGuideByID(id);
    }
    setmodal_standard_update(!modal_standard_update);
    removeBodyCss();
  }

  function removeBodyCss() {
    document.body.classList.add("no_padding")
  }

  useEffect(() => {
    let pagings = [];

    fetch("https://tour-guide-masterdata.herokuapp.com/get.all.guide", {
      method: 'POST',
      headers: {
        'Cache-Control': 'no-cache',
        'Accept': 'application/json;charset=UTF-8',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.5BG9SEVOGo_xRhtT8IkyoSy60kPg8HM9Vpvb0TdNew4'
      },
      body: JSON.stringify({ limit: 10, offset: 0, params: {param:'', status:'ACTIVE'} })
    })
      .then((res) => res.json())
      .then((res) => {
        setGuides(res);
        setCurrentPage(0);
      });
  }, [])


  const breadcrumbItems = [
    { title: "TourGuide", link: "#" },
    { title: "Guide", link: "#" },
  ]
  

  useEffect(() => {
    props.setBreadcrumbItems('Data Tables', breadcrumbItems);
    if(Object.keys(guides).length>0){
      let postsArray = JSON.parse(JSON.stringify(guides.data.datas));
      let userData = [];
      console.log("GUIDE: ", guides);
      console.log("POSTS: ", postsArray);
      postsArray.map((item, index) => {
        item.action = <div className="button-items"><Button type="button" color="warning" id={item.guideID} className="waves-effect waves-light" type="button"
          onClick={() => {
            tog_standard_update(item.guideID)
          }}
          className="btn btn-primary waves-effect waves-light"
          data-toggle="modal"
          data-target="#myModal"><i className="mdi mdi-lead-pencil"></i></Button>
          <Button type="button" color="danger" id={item.guideID} className="waves-effect waves-light" 
          onClick={() => {
            setconfirm_alert(true)
            setGuide(item.guideID)
          }}><i className="mdi mdi-trash-can"></i></Button></div>;
        item.id = (
          <div style={{ fontWeight: "bold", fontSize: "1.2em" }}>{item.id}</div>
        );
        userData.push(item);
      });
      setUsersForRender(userData);
    } else {
      
    }
  }, [guides]);

  function getGuides(page) {
    fetch("https://tour-guide-masterdata.herokuapp.com/get.all.guide", {
      method: 'POST',
      headers: {
        'Cache-Control': 'no-cache',
        'Accept': 'application/json;charset=UTF-8',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.5BG9SEVOGo_xRhtT8IkyoSy60kPg8HM9Vpvb0TdNew4'
      },
      body: JSON.stringify({ limit: 10, offset: page, params: {param:'', status:'ACTIVE'} })
    })
      .then((res) => res.json())
      .then((res) => {
        setGuides(res);
      });
  }

  function getGuideByID(id) {
    fetch("https://tour-guide-masterdata.herokuapp.com/get.guide.by.id/" + id, {
      method: 'GET',
      headers: {
        'Cache-Control': 'no-cache',
        'Accept': 'application/json;charset=UTF-8',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.5BG9SEVOGo_xRhtT8IkyoSy60kPg8HM9Vpvb0TdNew4'
      },
    })
    .then((res) => res.json())
    .then((res) => {
      console.log("GET: " + JSON.stringify(res.data));
      setGuide(res.data.guideID);
      setName(res.data.guideName);
      setEmail(res.data.guideEmail);
      setKtp(res.data.guideKTPNumber);
      setMobileNumber(res.data.guidePhoneNumber);
    });
  }

  function updateGuide(id) {
    getGuideByID(id);

    fetch(`https://tour-guide-masterdata.herokuapp.com/update.guide`, {
      method: "PUT",
      headers: {
        'Cache-Control': 'no-cache',
        'Accept': 'application/json;charset=UTF-8',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.5BG9SEVOGo_xRhtT8IkyoSy60kPg8HM9Vpvb0TdNew4'
      },
      body: JSON.stringify({
        guideID: guide,
        guideName: name,
        guideEmail: email,
        guidePhoneNumber: mobileNumber,
        guideKTPNumber: ktp,
        guideAVGRating: 0.0,
        guidePhotoURL: "",
        guideDateOfBirth: null,
        guideJoinDate: null,
        guideLanguage: [],
        isBooked: false,
        guideGender: null,
        guideStatus: "ACTIVE",
        guideCreational: {
          createdBy: "ADMIN",
          createdDate: null,
          updatedBy: "ADMIN",
          updatedDate: null,
        },
      }),
    })
      .then(res => {
        if (res.ok) getGuides(currentPage);
      })
      .catch(err => console.error(err));
      setGuide("");
      setName("");
        setEmail("");
        setKtp("");
        setMobileNumber("");
  }

  function deleteGuide(id) {
    fetch(`https://tour-guide-masterdata.herokuapp.com/delete.guide`, {
      method: "DELETE",
      headers: {
        'Cache-Control': 'no-cache',
        'Accept': 'application/json;charset=UTF-8',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiJ9.5BG9SEVOGo_xRhtT8IkyoSy60kPg8HM9Vpvb0TdNew4'
      },
      body: JSON.stringify({ referenceID: id, requestBy: "ADMIN", requestDate: "11-11-2021 11:11:11" })
    })
      .then(res => {
        if (res.ok) getGuides(currentPage);
      })
      .catch(err => console.error(err));
  }

  const data = {
    columns: [
      {
        label: "ID",
        field: "guideID",
        sort: "asc",
        width: 150,
      },
      {
        label: "Name",
        field: "guideName",
        sort: "asc",
        width: 270,
      },

      {
        label: "Phone Number",
        field: "guidePhoneNumber",
        sort: "asc",
        width: 200,
      },
      {
        label: "Email",
        field: "guideEmail",
        sort: "asc",
        width: 200,
      },
      {
        label: "Action",
        field: "action",
        sort: "asc",
        width: 100,
      },
    ],
    rows: usersForRender,
  }

  return (
    <React.Fragment>
      
        <MetaTags>
          <title>Guide | TourGuide</title>
        </MetaTags>

          <Row>
            <Col className="col-12">
              <Card>
                <CardBody>
                  <CardTitle className="h4">Guide </CardTitle>
                  <p className="card-title-desc">
                    mdbreact DataTables has most features enabled by default, so
                    all you need to do to use it with your own tables is to call
                    the construction function:{" "}
                    <code>&lt;MDBDataTable /&gt;</code>.
                  </p>
                  <div className=" text-center">
                  <button
                    type="button"
                    onClick={() => {
                      tog_standard()
                    }}
                    className="btn btn-primary waves-effect waves-light"
                    data-toggle="modal"
                    data-target="#myModal"
                  >
                    Register New Guide
                      </button>
                  <Modal
                    size="lg"
                    isOpen={modal_standard}
                    toggle={() => {
                      tog_standard()
                    }}
                  >
                    <div className="modal-header">
                      <h5 className="modal-title mt-0" id="myModalLabel">
                        TourGuide Registration
                          </h5>
                      <button
                        type="button"
                        onClick={() => {
                          setmodal_standard(false)
                        }}
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div className="modal-body">
                          <div className="card-body">
                          <form noValidate="" action="#" method="get" className="needs-validation av-invalid" onSubmit={handleSubmit}>
                          <div className="mb-3 row">
                          <label htmlFor="example-text-input" className="col-md-2 col-form-label">
                          Name
                          </label>
                          <div className="col-md-10">
                          <input className="form-control" type="text" value={name} onChange={function (event) {
                            setName(event.target.value);
                          }}/>
                          </div>
                          </div>
                          <div className="mb-3 row">
                          <label htmlFor="example-text-input" className="col-md-2 col-form-label">
                          KTP Number
                          </label>
                          <div className="col-md-10">
                          <input className="form-control" type="text" value={ktp} onChange={function (event) {
                            setKtp(event.target.value);
                          }}/>
                          </div>
                          </div>
                          <div className="mb-3 row">
                          <label htmlFor="example-text-input" className="col-md-2 col-form-label">
                          Phone Number
                          </label>
                          <div className="col-md-10">
                          <input className="form-control" type="text" value={mobileNumber} onChange={function (event) {
                            setMobileNumber(event.target.value);
                          }}/>
                          </div>
                          </div>
                          <div className="mb-3 row">
                          <label htmlFor="example-text-input" className="col-md-2 col-form-label">
                          Email
                          </label>
                          <div className="col-md-10">
                          <input className="form-control" type="text" value={email} onChange={function (event) {
                            setEmail(event.target.value);
                          }}/>
                          </div>
                          </div>
                          <div className="mb-3 row">
                          <label htmlFor="example-date-input" className="col-md-2 col-form-label">
                            Date of Birth
                              </label>
                          <div className="col-md-10">
                            <input
                            className="form-control"
                              type="date"
                              value="2019-08-19"
                              id="example-date-input"
                              onChange={function (event) {
                                setActivity(event.target.value);
                              }}
                            />
                          </div>
                          </div>
                          <div className="mb-3 row">
                          <label htmlFor="example-date-input" className="col-md-2 col-form-label">
                            Date Joined
                              </label>
                          <div className="col-md-10">
                            <input
                            className="form-control"
                              type="date"
                              value="2019-08-19"
                              id="example-date-input"
                              onChange={function (event) {
                                setActivity(event.target.value);
                              }}
                            />
                          </div>
                          </div>

                          <div className="mb-3 row">
                          <label className="col-md-2 col-form-label">Gender</label>
                          <div className="col-md-10">
                            <select className="form-control">
                              <option>Choose a Gender</option>
                              <option>Male</option>
                              <option>Female</option>
                            </select>
                          </div>
                          </div>
                          
                          <div className="modal-footer">
                      <button
                        type="button"
                        onClick={() => {
                          tog_standard()
                        }}
                        className="btn btn-secondary waves-effect"
                        data-dismiss="modal"
                      >
                        Close
                          </button>
                          <button type="submit" className="btn btn-primary">Submit form</button>
                    </div>
                          </form>
                          </div>
                    </div>
                    
                  </Modal>
                  <Modal
                    size="lg"
                    isOpen={modal_standard_update}
                    toggle={() => {
                      tog_standard_update()
                    }}
                  >
                    <div className="modal-header">
                      <h5 className="modal-title mt-0" id="myModalLabel">
                        TourGuide Data Update
                          </h5>
                      <button
                        type="button"
                        onClick={() => {
                          setmodal_standard_update(false)
                        }}
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div className="modal-body">
                          <div className="card-body">
                          <form noValidate="" action="#" method="get" className="needs-validation av-invalid" onSubmit={handleUpdate}>
                          <div className="mb-3 row">
                          <label htmlFor="example-text-input" className="col-md-2 col-form-label">
                          Name
                          </label>
                          <div className="col-md-10">
                          <input className="form-control" type="text" value={name} onChange={function (event) {
                            setName(event.target.value);
                          }}/>
                          </div>
                          </div>
                          <div className="mb-3 row">
                          <label htmlFor="example-text-input" className="col-md-2 col-form-label">
                          KTP Number
                          </label>
                          <div className="col-md-10">
                          <input className="form-control" type="text" value={ktp} onChange={function (event) {
                            setKtp(event.target.value);
                          }}/>
                          </div>
                          </div>
                          <div className="mb-3 row">
                          <label htmlFor="example-text-input" className="col-md-2 col-form-label">
                          Phone Number
                          </label>
                          <div className="col-md-10">
                          <input className="form-control" type="text" value={mobileNumber} onChange={function (event) {
                            setMobileNumber(event.target.value);
                          }}/>
                          </div>
                          </div>
                          <div className="mb-3 row">
                          <label htmlFor="example-text-input" className="col-md-2 col-form-label">
                          Email
                          </label>
                          <div className="col-md-10">
                          <input className="form-control" type="text" value={email} onChange={function (event) {
                            setEmail(event.target.value);
                          }}/>
                          </div>
                          </div>
                          <div className="mb-3 row">
                          <label htmlFor="example-date-input" className="col-md-2 col-form-label">
                            Date of Birth
                              </label>
                          <div className="col-md-10">
                            <input
                            className="form-control"
                              type="date"
                              value="2019-08-19"
                              id="example-date-input"
                              onChange={function (event) {
                                setActivity(event.target.value);
                              }}
                            />
                          </div>
                          </div>
                          <div className="mb-3 row">
                          <label htmlFor="example-date-input" className="col-md-2 col-form-label">
                            Date Joined
                              </label>
                          <div className="col-md-10">
                            <input
                            className="form-control"
                              type="date"
                              value="2019-08-19"
                              id="example-date-input"
                              onChange={function (event) {
                                setActivity(event.target.value);
                              }}
                            />
                          </div>
                          </div>

                          <div className="mb-3 row">
                          <label className="col-md-2 col-form-label">Gender</label>
                          <div className="col-md-10">
                            <select className="form-control">
                              <option>Choose a Gender</option>
                              <option>Male</option>
                              <option>Female</option>
                            </select>
                          </div>
                          </div>
                          
                          <div className="modal-footer">
                      <button
                        type="button"
                        onClick={() => {
                          tog_standard_update()
                        }}
                        className="btn btn-secondary waves-effect"
                        data-dismiss="modal"
                      >
                        Close
                          </button>
                          <button type="submit" className="btn btn-primary">Submit form</button>
                    </div>
                          </form>
                          </div>
                    </div>
                    
                  </Modal>
                </div>
                  <MDBDataTable 
                  responsive 
                  bordered 
                  data={data}
                  paging={false}
                    />
                    <nav aria-label="...">
                      <nav className="" aria-label="pagination">
                        <ul className="pagination justify-content-end">
                          <li className="page-item">
                            <a href="#" onClick={() => {
                              setCurrentPage(currentPage-1);
                              getGuides(currentPage-1);
                            }} className="page-link">Previous</a>
                          </li>
                          <li className="page-item">
                            <a href="#" className="page-link">{currentPage+1}</a>
                          </li>
                          <li className="page-item">
                            <a href="#" onClick={() => {
                              setCurrentPage(currentPage+1);
                              getGuides(currentPage+1);
                            }} className="page-link">Next</a>
                          </li>
                        </ul>
                      </nav>
                    </nav>
                  </CardBody>
              </Card>
            </Col>
            {custom_div1 ? (
              <SweetAlert
                title="Guide data registered successfully."
                timeout={2000}
                style={{
                  position: "absolute",
                  top: "0px",
                  right: "0px",
                }}
                showCloseButton={false}
                showConfirm={false}
                success
                onConfirm={() => {
                  setcustom_div1(false)
                }}
              ></SweetAlert>
            ) : null}
            {custom_div2 ? (
              <SweetAlert
                title="Guide data deleted successfully."
                timeout={2000}
                style={{
                  position: "absolute",
                  top: "0px",
                  right: "0px",
                }}
                showCloseButton={false}
                showConfirm={false}
                success
                onConfirm={() => {
                  setcustom_div2(false)
                }}
              ></SweetAlert>
            ) : null}
            {custom_div3 ? (
              <SweetAlert
                title="Guide data updated successfully."
                timeout={2000}
                style={{
                  position: "absolute",
                  top: "0px",
                  right: "0px",
                }}
                showCloseButton={false}
                showConfirm={false}
                success
                onConfirm={() => {
                  setcustom_div3(false)
                }}
              ></SweetAlert>
            ) : null}
            {confirm_alert ? (
              <SweetAlert
                title="Are you sure?"
                warning
                showCancel
                confirmButtonText="Yes, delete it!"
                confirmBtnBsStyle="success"
                cancelBtnBsStyle="danger"
                onConfirm={() => {
                  deleteGuide(guide)
                  setconfirm_alert(false)
                  setcustom_div2(true);
                }}
                onCancel={() => setconfirm_alert(false)}
              >
                You won't be able to revert this!
              </SweetAlert>
            ) : null}
          </Row>
        
    </React.Fragment>
  )
}

export default connect(null, { setBreadcrumbItems })(DatatableTables);